define([
    'underscore',
    'backbone',
    'collections/TracksCollection',
    'models/TrackModel'
], function( _, Backbone, TracksCollection, TrackModel) {

    var ArtistModel = Backbone.Model.extend({
        defaults : {
            tracks : new TracksCollection()
        },

        initialize: function() {

            //console.log(this.get('tracks'));
        }
    });

    return ArtistModel;
});