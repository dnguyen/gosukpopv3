define([
    'underscore',
    'backbone'
], function( _, Backbone ) {

    var TrackModel = Backbone.Model.extend({
        initialize: function() {
        },

        CleanTitleAndArtist: function() {
            var newTitle = this.get('shortenedTitle');
            var newArtist = this.get('artistName');
            if (this.get('shortenedTitle').length > 15)
                this.set('shortenedTitle', newTitle.substring(0, 15)+"...");
            else
                this.set('shortenedTitle', newTitle);

            var cleanTitle = encodeURI(this.get('title')).replace(/%20/g, "+").replace(/[^a-zA-Z0-9-_]/g, "");
            this.set('cleanTitle', cleanTitle);
            var cleanArtist = encodeURI(this.get('artistName')).replace(/%20/g, "+").replace(/[^a-zA-Z0-9-_]/g, "");
            this.set('cleanArtistName', cleanArtist);
        },

        GetCleanTitle : function() {
            return encodeURI(this.get('title')).replace(/%20/g, "+").replace(/[^a-zA-Z0-9-_]/g, "");
        }

    });

    return TrackModel;

});