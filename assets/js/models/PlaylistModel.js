define([
    'underscore',
    'backbone',
    'models/TrackModel',
    'collections/TracksCollection'
], function( _, Backbone, TrackModel, TracksCollection) {

    var PlaylistModel = Backbone.Model.extend({
        initialize: function() {
            this.set('tracks', new TracksCollection());
            this.set('currentTrackIndex', 0);
        },

        loadTracks: function(playlistData) {
            if (playlistData.tracks.length > 0) {
                this.set({playlistTrackCount : 1 });

                var this_ = this;
                _.each(playlistData.tracks, function(track) {
                    this_.addTrack(track);
                    this_.set({playlistTrackCount : this_.get('playlistTrackCount') + 1});
                });
            }
        },

        addTrack: function(trackData) {
            var trackModel = new TrackModel({
                id : trackData.id,
                playlistTrackId: trackData.playlistTrackId,
                videoid : trackData.videoId,
                title : trackData.title,
                shortenedTitle : trackData.title,
                artist : trackData.artist,
                artistName : trackData.name,
                trackCount : this.get('playlistTrackCount')
            });
            trackModel.CleanTitleAndArtist();

            this.get('tracks').add(trackModel);
        }
    });

    return PlaylistModel;
});