define([
    'underscore',
    'backbone',
    'models/TrackModel'
], function(_, Backbone, Track) {

    var TracksCollection = Backbone.Collection.extend( {
        model: Track
    });


    return TracksCollection;
});