define([
    'underscore',
    'backbone',
    'models/ArtistModel'
], function(_, Backbone, Artist) {
    var ArtistsCollection = Backbone.Collection.extend({
        model: Artist
    });

    return ArtistsCollection;
});