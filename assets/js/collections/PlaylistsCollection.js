define([
    'underscore',
    'backbone',
    'models/PlaylistModel'
], function(_, Backbone, PlaylistModel) {
    var PlaylistsCollection = Backbone.Collection.extend({
        model: PlaylistModel
    });

    return PlaylistsCollection;
});