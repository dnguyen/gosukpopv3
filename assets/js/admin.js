$(document).ready(function() {
    $('.delete-track').click(function() {
        var trackid = $(this).parent().parent().attr('data-track-id');
        $.ajax({
            url: base_url+"gadmin/delete_track",
            type: 'POST',
            data: 'id='+trackid,
            dataType: 'json',
            success: function(output) {
                $('.track-row[data-track-id="'+trackid+'"]').remove();
            },
            error: function(error) {
                console.log(error);
            }
        });
    });

    $('.get-lastfm-desc').click(function() {
        var artistName = $(this).attr('artist-name');
        console.log(artistName);
        $.ajax({
            url: base_url+"gadmin/getArtistDescription",
            type: 'POST',
            data: 'name='+artistName,
            dataType: 'json',
            success: function(output) {
                tinyMCE.activeEditor.setContent('');
                var newContent = '<img src="'+output.image+'" /></br>' + output.description;
                tinyMCE.activeEditor.setContent(newContent);
            },
            error: function(error) {
                console.log(error);
            }
        });
    });

    $('#update-artist-btn').click(function() {
        var artistId = $('#artist-id-input').val();
        var artistName = $('#artist-name-input').val();
        var description = tinyMCE.activeEditor.getContent();
        $.ajax({
            url: base_url+"gadmin/update_artist",
            type: 'POST',
            data: { id : artistId, name : artistName, desc : description },
            dataType: 'json',
            success: function(output) {
                window.location = base_url+'gadmin/artists';
            },
            error: function(error) {
                console.log(error);
            }
        });
    });
});