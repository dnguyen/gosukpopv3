require.config({
    shim: {
        'underscore': {
            exports: '_'
        },
        'backbone': {
            deps: [
                'underscore',
                'jquery'
            ],
            exports: 'Backbone'
        },

        'backbone_query_parameters' : {
            deps: [
                'underscore',
                'backbone'
            ],
            exports: 'Backbone_Query_Parameters'
        },
        'gosulib': {
            deps: [
                'underscore'
            ],
            exports: 'gosu'
        },
        'bootstrap': {
            deps: [
                'jquery'
            ]
        },
        'pnotify': {
            deps: [
                'jquery'
            ]
        }
    },
    paths: {
        jquery: 'lib/jquery/jquery-1.8.2.min',
        bootstrap: 'lib/bootstrap/bootstrap',
        underscore: 'lib/underscore/underscore-min',
        backbone: 'lib/backbone/backbone',
        text: 'lib/require/text',
        gosulib: 'lib/gosulib/gosulib',
        ytapi: 'lib/ytapi/player',
        pnotify: 'lib/jquery/jquery.pnotify.min',
        backbone_query_parameters: 'lib/backbone/backbone.queryparams'
    }
});

require([
    'views/app',
    'routers/router'
], function(AppView, AppRouter) {
    var appRouter = new AppRouter();
    window.AppView = new AppView();
    Backbone.history.start({pushState: false});
});