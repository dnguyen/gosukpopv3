define([
    'jquery',
    'underscore',
    'backbone',
    'backbone_query_parameters',
    'require'
], function($, _, Backbone, Backbone_Query_Parameters, require) {

    var AppRouter = Backbone.Router.extend({
        routes: {
            "" : "explore",
            "explore" : "explore",
            "tracks" : "tracks",
            "tracks/:page" : "tracks",
            "tracks/search/:input" : "searchTracks",
            "track/*name/:id" : "track",
            "login" : "login",
            "signup" : "signup",
            "artists" : "artists",
            "artists/:page" : "artists",
            "artists/search/*input" : "searchArtists",
            "artist/*name/:id" : "artist",
            "playlists" : "playlists",
            "playlist/*name/*id" : "playlist"
        },

        routeDoesNotExist : function() {
            console.log("404 error");
        },

        explore : function() {
            var ExploreViewRoute = require(['views/ExploreView'], function (ExploreView) {
                console.log("explore route");
                window.AppView.switchMainView(new ExploreView());
            });

        },

        login : function() {
            var LoginViewRoute = require(['views/auth/LoginView'], function (LoginView) {
                console.log("login route");
                window.AppView.switchMainView(new LoginView());
            });
        },

        signup: function() {
            var SignUpViewRoute = require(['views/auth/RegisterView'], function (RegisterView) {
                console.log("sign up route");
                window.AppView.switchMainView(new RegisterView());
            });
        },

        tracks : function(page, params) {
            console.log("tracks route");
            var TrackViewRoute = require(['views/tracks/TracksView'], function (TracksView) {

                console.log("tracks route");
                // Default to sort by uploaded desc order.
                if (typeof params === 'undefined') {
                    params = { };
                    params.sort = 'uploaded';
                    params.order = 'desc';
                }
                window.AppView.switchMainView(
                    new TracksView({
                        page: page,
                        sortType : params.sort,
                        order : params.order
                    })
                );
            });
        },

        searchTracks : function(input, params) {
            var TrackViewRoute = require(['views/tracks/TracksView'], function (TracksView) {
                console.log("search tracks route");

                if (typeof params === 'undefined') {
                    params = { };
                    params.sort = 'uploaded';
                    params.order = 'desc';
                }

                window.AppView.switchMainView(
                    new TracksView({
                        isSearching: true,
                        searchTerms : input,
                        sortType : params.sort,
                        order : params.order
                    })
                );
            });
        },

        track : function(name, trackid) {
            var SingleTrackViewRoute = require(['views/tracks/SingleTrackView'], function (SingleTrackView) {
                console.log("Single track view rotue");
                console.log(name);
                console.log(trackid);
                window.AppView.switchMainView(new SingleTrackView( { name : name, trackid : trackid }));
            });
        },

        artists : function(page, params) {
            var ArtistsViewRoute = require(['views/artists/ArtistsView'], function (ArtistsView) {

                if (typeof params === 'undefined')
                    params = { };

                if (typeof params.sort === 'undefined')
                    params.sort = 'name';
                if (typeof params.order === 'undefined')
                    params.order = 'desc';
                if (typeof params.gender === 'undefined')
                    params.gender = 'all';
                if (typeof params.artisttype === 'undefined')
                    params.artisttype = 'all';
                //}
                window.AppView.switchMainView(
                    new ArtistsView({
                        page : page,
                        sortType : params.sort,
                        order : params.order,
                        gender : params.gender,
                        artistType : params.artisttype
                    })
                );
            });
        },

        searchArtists : function(input) {
            var ArtistsViewRoute = require(['views/artists/ArtistsView'], function(ArtistsView) {
                window.AppView.switchMainView(new ArtistsView({ isSearching : true, searchTerms : input }));
            });
        },

        artist: function(name, artistid) {
            var SingleArtistViewRoute = require(['views/artists/SingleArtistView'], function (SingleArtistView) {
                window.AppView.switchMainView(new SingleArtistView(name, artistid));
            });
        },

        playlists: function() {
            var PlaylistsViewRoute = require(['views/playlists/PlaylistsView'], function (PlaylistsView) {
                window.AppView.switchMainView(new PlaylistsView());
            });
        },

        playlist: function(name, playlistid) {
            console.log(name);
            console.log(playlistid);
            var SinglePlaylistViewRoute = require(['views/playlists/SinglePlaylistView'], function (SinglePlaylistView) {
                window.AppView.switchMainView(new SinglePlaylistView({ name : name, playlistid : playlistid }));
            });
        }
    });

    return AppRouter;
});