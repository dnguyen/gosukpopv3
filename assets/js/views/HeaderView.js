define([
    'jquery',
    'underscore',
    'backbone',
    'models/UserModel',
    'text!templates/HeaderTemplate.html'
], function($, _, Backbone, UserModel, HeaderTemplate) {

    var HeaderView = Backbone.View.extend({
        el: '#header',
        template: _.template(HeaderTemplate),

        events: {
            'click #sign-in-button' : 'showLoginModal',
            'click #search-btn' : 'search',
            'click #sign-out-btn' : 'signOut'
        },

        initialize: function() {
            console.log('header_view init');
        },

        render: function() {
            $(this.el).html(this.template(this.model.toJSON()));
            return this;
        },

        showLoginModal: function(e) {
            console.log("show login");
        },

        search: function(e) {
            console.log("searching");
        },

        signOut: function() {
            var signOutRequest = $.ajax({
                type: 'DELETE',
                url: 'http://10.0.0.6/gosukpop-api/public/auth',
                dataType: 'json'
            });

            $.when(signOutRequest).done(function(data) {
                Backbone.history.loadUrl(Backbone.history.fragment);
            });
        }
    });

    return HeaderView;

});