define([
    'jquery',
    'bootstrap'
], function($) {
    $(document).ready(function() {
        $('#account-options-dropdown').click(function(event) {
            $('.dropdown-toggle').dropdown();
        });

        $('.track-row').hover(function(event) {
            var id = $(this).attr('data-track-id');
            $('#track-option-'+id+' ul').show();
        }, function(event) {
            var id = $(this).attr('data-track-id');
            $('#track-option-'+id+' ul').hide();
        });

        $('.track-row').click(function(event) {
            var id = $(this).attr('data-track-id');
            var name = encodeURI($(this).attr('title')).replace(/%20/g, "+").replace(/[^a-zA-Z0-9-_]/g, "");
            window.location.href = base_url+'track/'+name+'/'+id;
        });

        $('.track-options li').click(function(event) {
            event.stopPropagation();
            var trackid = $(this).parent().parent().parent().parent().attr('data-track-id');
            console.log(trackid);
        });


        $('.playlist-row').click(function(event) {
            var id = $(this).attr('data-playlist-id');
            var name = encodeURI($(this).attr('title')).replace(/%20/g, "+").replace(/[^a-zA-Z0-9-_]/g, "");
            //window.location.href = base_url+'playlist/'+name+'/'+id;
            console.log(id + " " + name);
        });
    });
});