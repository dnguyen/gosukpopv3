define([
    'jquery',
    'underscore',
    'backbone',
    'models/TrackModel',
    'collections/TracksCollection',
    'views/explore/NewReleaseView',
    'views/explore/PopularTrackView',
    'text!templates/ExploreHomeTemplate.html'
], function($, _, Backbone, TrackModel, TracksCollection, NewReleaseView, PopularTrackView, ExploreHomeTemplate) {

    var ExploreView = Backbone.View.extend({
        tagName: 'section',
        template: _.template(ExploreHomeTemplate),
        events: {
        },

        initialize: function() {
            window.AppView.eventAggregator.trigger('domchange:title', window.AppView.base_title + "Explore");

            this.NewReleasesCollection = new TracksCollection();
            this.NewReleasesCollection.bind('add', this.AddNewRelease);

            this.PopularTracksCollection = new TracksCollection();
            this.PopularTracksCollection.bind('add', this.AddPopularTrack);

            $(this.el).attr('id', 'explore-view').html(this.template());
        },

        render: function() {
            $('#main-content').append(this.el);
            this.Load();
        },

        Load: function() {
            var this_ = this;

            $.ajax({
                type: "GET",
                url: "http://10.0.0.6/gosukpop-api/public/NewTrackReleases",
                data: { count : 12 },
                dataType: 'json',
                success: function(data) {
                    _.each(data, function(track) {
                        var newTrack = new TrackModel ({
                                id : track.id,
                                title : track.title,
                                shortenedTitle : track.title,
                                artist : track.artist,
                                artistName : track.name,
                                uploaded : track.uploaded,
                                videoId : track.videoId,
                                viewCount : track.viewCount
                        });
                        newTrack.CleanTitleAndArtist();
                        this_.NewReleasesCollection.add(newTrack);
                    });
                }
            });

            $.ajax({
                type: "GET",
                url: "http://10.0.0.6/gosukpop-api/public/MostViewedTracks",
                data: { count : 5 },
                dataType: 'json',
                success: function(data) {
                    _.each(data, function(track) {
                        var newTrack = new TrackModel({
                                id : track.id,
                                title : track.title,
                                shortenedTitle : track.title,
                                artist : track.artist,
                                artistName : track.name,
                                uploaded : track.uploaded,
                                videoId : track.videoId,
                                viewCount : track.viewCount
                            });
                        newTrack.CleanTitleAndArtist();
                        this_.PopularTracksCollection.add(newTrack);
                    });
                }
            });
         },

        AddNewRelease: function(newRelease) {
            var releaseView = new NewReleaseView({model: newRelease });
            $('#new-releases-list').append(releaseView.render().el);
        },

        AddPopularTrack: function(track) {
            var popularTrackView = new PopularTrackView({model: track });
            $('#popular-tracks-list').append(popularTrackView.render().el);
        }
    });

    return ExploreView;
});