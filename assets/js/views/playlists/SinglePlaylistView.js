define([
    'jquery',
    'underscore',
    'backbone',
    'models/PlaylistModel',
    'models/TrackModel',
    'models/VideoPlayerModel',
    'views/playlists/VideoPlayerView',
    'views/playlists/PlaylistTrackRowView',
    'text!templates/SinglePlaylistTemplate.html'
], function($, _, Backbone, PlaylistModel, TrackModel, VideoPlayerModel, VideoPlayerView, PlaylistTrackRowView, SinglePlaylistTemplate) {

    var SinglePlaylistView = Backbone.View.extend({

        template: _.template(SinglePlaylistTemplate),

        initialize: function(params) {
            console.log("init single playlsit view");
            this.videoPlayerView = new VideoPlayerView( this, { model: new VideoPlayerModel() });

            this.model = new PlaylistModel({
                        id : params.playlistid,
                        name : params.name
            });
        },

        render: function() {
            console.log("render");
            var this_ = this;

            $.when(this.prepareData()).done(function() {
                console.log("data ready. render single playlist view");

                this_.model.bind('change:currentTrack', this_.changeTrack, this_);
                this_.model.bind('change:currentTrackIndex', this_.changeTrackIndex, this_);

                window.AppView.eventAggregator.trigger('domchange:title', window.AppView.base_title + "Playlist - " + this_.model.get('name'));

                $(this_.el).attr('id', 'single-playlist-view').html(this_.template(this_.model.toJSON()));
                $('#main-content').append(this_.el);

                // Render video player
                var firstTrack = this_.model.get('tracks').at(0);
                this_.videoPlayerView.render(firstTrack.get('videoid'));

                // Render playlist tracks
                this_.renderTracksCollection();
            });
        },

        /*
            Renders tracks table
         */
        renderTracksCollection : function() {
            var container = document.createDocumentFragment();
            $('#tracks-table tbody').html('');
            var this_ = this;
            this.model.get('tracks').forEach(function(track) {
                var TrackItem = new PlaylistTrackRowView({ model: track });
                TrackItem.on("playBtnClicked", this_.playTrack, this_);

                container.appendChild(TrackItem.render().el);
            });
            $('#tracks-table tbody').append(container);
        },

        prepareData: function() {
            var this_ = this;

            return $.ajax({
                type: "GET",
                url: "http://10.0.0.6/gosukpop-api/public/playlists/" + this.model.get('id'),
                dataType: 'json',
                success: function(data) {
                    console.log(data);
                    this_.model.loadTracks(data);
                }
            });
        },

        // Events from track options
        playTrack: function(track) {
            console.log("Play track" + track.get('videoid'));
            this.model.set({
                currentTrack: track.get('videoid'),
                currentTrackIndex : this.model.get('tracks').indexOf(track)
            });
        },

        changeTrack: function() {
            console.log("Track changed");
            $('#ytplayer').remove();
            $('#playlist-module-container').prepend('<div id="ytplayer"></div>');
            this.videoPlayerView.render(this.model.get('currentTrack'));
        },

        changeTrackIndex: function() {
            this.model.set('currentTrack', this.model.get('tracks').at(this.model.get('currentTrackIndex')).get('videoid'));
        },

        // YouTube player events
        onPlayerReady: function(e) {
            console.log("Player ready");
            e.target.playVideo();
        },

        onPlayerStateChange: function(e) {
            if (e.data == YT.PlayerState.ENDED) {
                console.log("track ended");
                // Restart the playlist from the beginning when it reaches the end.
                if (this.AppView.main.model.get('currentTrackIndex') + 1 >= this.AppView.main.model.get('tracks').length) {
                   this.AppView.main.model.set('currentTrackIndex', 0);
                } else {
                    this.AppView.main.model.set('currentTrackIndex', this.AppView.main.model.get('currentTrackIndex') + 1);
                }
            }
        }
    });

    return SinglePlaylistView;

});