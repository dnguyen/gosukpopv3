define([
    'jquery',
    'underscore',
    'backbone',
    'ytapi'
    ], function($, _, Backbone, YTApi) {

        var VideoPlayerView = Backbone.View.extend({

            initialize: function(parentView) {
                this.parent = parentView;
                console.log("Initialize video player view");
            },

            render: function(videoid) {
                this.player = new YT.Player('ytplayer', {
                        height: '500',
                        width: '100%',
                        videoId: videoid,
                        events: {
                            'onReady' : this.parent.onPlayerReady,
                            'onStateChange' : this.parent.onPlayerStateChange
                        }
                });
            }
        });

        return VideoPlayerView;
    }
);