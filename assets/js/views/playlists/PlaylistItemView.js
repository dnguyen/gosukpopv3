define([
    'jquery',
    'underscore',
    'backbone',
    'text!templates/PlaylistItemTemplate.html'
], function($, _, Backbone, PlaylistItemTemplate) {

    var PlaylistItemView = Backbone.View.extend({

        tagName: 'tr',
        template: _.template(PlaylistItemTemplate),
        events: {
            'click' : 'goToPlaylistPage'
        },

        initialize: function() {

        },

        render: function() {
            $(this.el).attr('class', 'playlist-row').attr('id', this.model.get('uniqueid')).attr('data-playlist-id', this.model.get('id')).attr('title', this.model.get('name')).html(this.template(this.model.toJSON()));
            return this;
        },

        goToPlaylistPage: function(e) {
            var playlistId = this.model.get('uniqueid');
            var playlistName = encodeURI($(e.currentTarget).attr('title')).replace(/%20/g, "+").replace(/[^a-zA-Z0-9-_]/g, "");

            window.location = '#/playlist/' + playlistName + '/' + playlistId;
        }
    });

    return PlaylistItemView;
});