define([
    'jquery',
    'underscore',
    'backbone',
    'views/playlists/PlaylistItemView',
    'models/PlaylistModel',
    'collections/PlaylistsCollection',
    'text!templates/PlaylistsTemplate.html',
    'pnotify'
], function($, _, Backbone, PlaylistItemView, PlaylistModel, PlaylistsCollection, PlaylistsTemplate) {

    var PlaylistsView = Backbone.View.extend({

        tagName: 'section',
        events: {
            'click #create-playlist-btn' : 'createPlaylist'
        },
        template: _.template(PlaylistsTemplate),

        initialize: function() {
            console.log("playlists view init");
            window.AppView.eventAggregator.trigger('domchange:title', window.AppView.base_title + "Playlists");
            this.Playlists = new PlaylistsCollection();
            this.Playlists.bind('add', this.addPlaylist);
        },

        render: function() {
            console.log('playlists render');
            this.loadPlaylists();
            $(this.el).attr('id', 'playlists-view').html(this.template(window.AppView.$header.model.toJSON()));
            $('#main-content').append(this.el);
        },

        addPlaylist: function(playlist) {
            var PlaylistItem = new PlaylistItemView({ model: playlist });
            $('#playlist-table tbody').append(PlaylistItem.render().el);
        },

        loadPlaylists: function() {
            var this_ = this;
            $.ajax({
                type: "GET",
                url: "http://10.0.0.6/gosukpop-api/public/playlists",
                dataType: 'json',
                success: function(data) {
                    _.each(data.playlists, function(playlist) {
                        var newPlaylist = new PlaylistModel ({
                                id : playlist.id,
                                name : playlist.playlistName,
                                user : playlist.userid,
                                uniqueid : playlist.uniqueid
                        });
                        this_.Playlists.add(newPlaylist);
                    });
                },
                error: function(data) {
                    console.log("error");
                    console.log(data);
                }
            });
        },

        createPlaylist: function() {
            console.log("create playlist");
            var this_ = this;
            var playlistName = $('#create-playlist-textbox').val();

            if (playlistName !== '') {
                if (window.AppView.userModel.get('loggedin')) {
                    var createPlaylistRequest = $.ajax({
                        type: 'POST',
                        url: 'http://localhost/gosukpopv3-app/playlists/' + playlistName,
                        dataType: 'json'
                    });

                    $.when(createPlaylistRequest).done(function(data) {
                        // If playlist was created successfully, display success message.
                        if (data) {
                             $.pnotify({
                                text: 'Created playlist: ' + playlistName,
                                type: 'success',
                                delay: 3000
                            });

                             var newPlaylist = new PlaylistModel({
                                id : data.id,
                                name : data.playlistName,
                                user : data.userid,
                                uniqueid : data.uniqueid
                             });

                             if (data.public)
                                this_.Playlists.add(newPlaylist);

                        } else {
                             $.pnotify({
                                text: 'Failed to create playlist: ' + playlistName,
                                type: 'error',
                                delay: 3000
                            });
                        }
                    });
                } else {
                }
            }
        }
    });

    return PlaylistsView;
});