define([
    'jquery',
    'underscore',
    'backbone',
    'text!templates/PlaylistTrackRowTemplate.html'
], function($, _, Backbone, PlaylistTrackRowTemplate) {

    var PlaylistTrackRowView = Backbone.View.extend({
        tagName: 'tr',
        template: _.template(PlaylistTrackRowTemplate),
        events: {
            'mouseenter' : 'showTrackOptions',
            'mouseleave' : 'hideTrackOptions',
            'click .play-playlist-track' : 'playTrack'
        },

        initialize: function() {
        },

        render: function() {
            $(this.el).attr('class', 'track-row').attr('id', this.model.get('uniqueid')).attr('data-track-id', this.model.get('id')).html(this.template(this.model.toJSON()));
            return this;
        },

        showTrackOptions: function(e) {
            var trackId = $(e.currentTarget).attr('data-track-id');
            $('#track-option-' + trackId + ' div').show();
            // Store track id to remove the options when not hovering over track anymore.
            this.prevTrackId = trackId;
        },

        hideTrackOptions: function(e) {
            $('#track-option-' + this.prevTrackId + ' div').hide();
        },

        playTrack: function(e) {
            this.trigger("playBtnClicked", this.model);
        }
    });

    return PlaylistTrackRowView;

});