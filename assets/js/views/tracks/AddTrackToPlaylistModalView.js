define([
    'jquery',
    'underscore',
    'backbone',
    'bootstrap',
    'text!templates/AddTrackToPlaylistTemplate.html',
    'pnotify'
], function($, _, Backbone, bootstrap, AddTrackToPlaylistTemplate, pnotify) {

    var AddTrackToPlaylistModalView = Backbone.View.extend({
        events: {
            'hidden' : 'hide',
            'click #close-btn' : 'close',
            'click #add-btn' : 'add'
        },
        template: _.template(AddTrackToPlaylistTemplate),

        render: function() {
            $(this.el).html(this.template(this.model.toJSON()));
            return this;
        },

        show: function() {
            $(document.body).append(this.render().el);
            $('#add-track-to-playlist-modal').attr('data-track-id', this.model.get('id'));

            var playlists;
            // If the user is logged in, get playlists from server.
            // If not logged in, use localStorage
            if (window.AppView.userModel.get('loggedin')) {
                console.log("logged in");

                playlists = $.ajax({
                    type: 'GET',
                    url: 'user/playlists',
                    dataType: 'json'
                });
            } else {

            }

            $.when(playlists).done(function(data) {
                console.log(data);
                var optionsHtml = "";
                _.each(data, function(playlist) {
                    optionsHtml += '<option value="' + playlist.uniqueid + '">' + playlist.playlistName + '</option>';
                });
                $('#user-playlists-dropdown').append(optionsHtml);
                $('#add-track-to-playlist-modal').modal( {show : true, backdrop: true });
            });

        },

        close: function() {
            $('#add-track-to-playlist-modal').modal('hide');
        },

        hide: function() {
            this.remove();
        },

        add: function() {
            console.log("Add track to playlist");
            var trackId = $('#add-track-to-playlist-modal').attr('data-track-id');
            var playlistId = $('#user-playlists-dropdown').val();

            var addTrack = $.ajax({
                type: 'POST',
                url: 'http://localhost/gosukpopv3-app/playlists/' + playlistId + '/tracks/' + trackId,
                dataType: 'json'
            });

            var this_ = this;
            $.when(addTrack).done(function(data) {
                // If insert was successful, close modal and display notification.
                if (data) {
                    this_.close();
                    $.pnotify({
                        title: 'Added track',
                        text: 'Added track to playlist.',
                        type: 'success',
                        delay: 3000
                    });
                } else {
                    $('.modal-body .alert').remove();
                    $('.modal-body').prepend('<div class="alert alert-error"><i class="icon-exclamation-sign"></i> An error has occurred. Please try again.</div>');
                }
            });
        }
    });
    return AddTrackToPlaylistModalView;

});