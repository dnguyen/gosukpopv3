define([
    'jquery',
    'underscore',
    'backbone',
    'models/TrackModel',
    'views/tracks/AddTrackToPlaylistModalView',
    'text!templates/SingleTrackTemplate.html'
], function($, _, Backbone, TrackModel, AddTrackToPlaylistModalView, SingleTrackTemplate) {

    var SingleTrackView = Backbone.View.extend({

        tagName: 'section',
        template: _.template(SingleTrackTemplate),
        events: {
            'click #add-btn' : 'addToPlaylist'
        },

        initialize: function(params) {
            this.model = new Backbone.Model;

            this.model.set('trackid', params.trackid);
            this.model.set('name', params.name);
        },

        render: function() {
            var this_ = this;
            $.when(this_.prepareData()).done(function(data) {
                var trackData = this_.model.get('trackData');

                window.AppView.eventAggregator.trigger('domchange:title', window.AppView.base_title + trackData.get('artistName') + " - " + trackData.get('title'));
                $(this_.el).attr('id', 'single-track-view').html(this_.template(this_.model.toJSON()));
                $('#main-content').append(this_.el);
            });
        },

        prepareData: function() {
            var this_ = this;

            return $.ajax({
                type: "GET",
                url: "http://10.0.0.6/gosukpop-api/public/tracks/" + this_.model.get('trackid'),
                dataType: 'json',
                success: function(data) {
                    var trackModel = new TrackModel({
                        id : data[0].id,
                        title : data[0].title,
                        artist : data[0].artist,
                        artistName : data[0].name,
                        uploaded : data[0].uploaded,
                        videoId : data[0].videoId,
                        viewCount : data[0].viewCount
                    });

                    this_.model.set('trackData', trackModel);
                },
                error: function(data) {
                    console.log("error");
                }
            });
        },

        addToPlaylist: function() {
            var addTrackToPlaylistModal = new AddTrackToPlaylistModalView( { model: this.model });
            addTrackToPlaylistModal.show();
        }

    });

    return SingleTrackView;

});