define([
    'jquery',
    'underscore',
    'backbone',
    'gosulib',
    'models/TrackModel',
    'views/tracks/TrackItemView',
    'collections/TracksCollection',
    'text!templates/TracksTemplate.html'
], function($, _, Backbone, gosu, TrackModel, TrackItemView, TracksCollection, TracksTemplate) {

    var TracksView = Backbone.View.extend({

        tagName: 'section',
        template: _.template(TracksTemplate),
        events: {
            'click #sort-btn' : 'applySort',
            'click #tracks-search-btn' : 'searchTracks',
            'change .sort-dropdown' : 'applySort',
            'change .sort-order-dropdown' : 'applySort'
        },

        initialize: function(options) {
            console.log("Tracksview init");
            window.AppView.eventAggregator.trigger('domchange:title', window.AppView.base_title + "Tracks");

            this.Tracks = new TracksCollection();
            this.model = new Backbone.Model();

            this.model.set('isSearching', options.isSearching);
            this.model.set('searchTerms', options.searchTerms);

            this.model.set('sortType', options.sortType);
            this.model.set('sortOrder', options.order);

            this.model.set('noSearchResults', false);
            this.model.set('searchResultCount', 0);

            if (options.page > 0) {
                this.model.set('page', parseInt(options.page, 10));
            } else {
                this.model.set('page', 1);
            }
        },

        render: function() {
            var this_ = this;
            // Prepare data from server before rendering dom.
            $.when(this_.prepareData()).done(function() {
                console.log("AJAX DONE: Resources ready");
                $(this_.el).attr('id', 'tracks-view').html(this_.template(this_.model.toJSON()));
                $('#main-content').append(this_.el);
                this_.renderTracksCollection();
            });
        },

        /*
            Renders tracks table
         */
        renderTracksCollection : function() {
            console.log("Rendering tracks collection");
            var container = document.createDocumentFragment();
            $('#tracks-table tbody').html('');
            this.Tracks.forEach(function(track) {
                var TrackItem = new TrackItemView({ model: track });
                container.appendChild(TrackItem.render().el);
            });
            $('#tracks-table tbody').append(container);
        },

        /*
            Request tracks data from server.
            If user is not doing a search then just request normal paginated track data.
            If user is making a search send search request to server.
         */
        prepareData : function() {
            console.log("settings loaded");
            var this_ = this;
            var ajaxRequest;

            if (!this_.model.get('isSearching')) {
                ajaxRequest = $.ajax({
                    type: "GET",
                    url: "http://10.0.0.6/gosukpop-api/public/tracks",
                    data: {
                        page : this_.model.get('page'),
                        sort : this_.model.get('sortType'),
                        order : this_.model.get('sortOrder')
                    },
                    dataType: 'json',
                    success: function(data) {
                        this_.model.set('pageCount', data.pageCount);
                        this_.Tracks.reset(gosu.GetTracksCollectionFromArray(data.tracks).models);
                    }
                });
            } else {
                //Doesn't work with paranthesis and some other symbols.
                var cleanedSearchTerms = encodeURI(this_.model.get('searchTerms')).replace(/20/g, "+");

                ajaxRequest = $.ajax({
                    type: 'GET',
                    url: 'http://10.0.0.6/gosukpop-api/public/tracks/search/' + this_.model.get('searchTerms'),
                    data: {
                        sort : this_.model.get('sortType'),
                        order : this_.model.get('sortOrder')
                    },
                    dataType: 'json',
                    success: function(data) {
                        this_.Tracks.reset();

                        if (data.length > 0) {
                            _.each(data, function(track) {
                                var newTrack = new TrackModel ({
                                        id : track.trackId,
                                        title : track.title,
                                        shortenedTitle : track.title,
                                        artist : track.artist,
                                        artistName : track.name,
                                        uploaded : track.uploaded,
                                        videoId : track.videoId,
                                        viewCount : track.viewCount
                                });
                                newTrack.CleanTitleAndArtist();
                                this_.Tracks.add(newTrack);
                            });
                            this_.model.set('searchResultCount', data.length);
                        } else {
                            this_.model.set('noSearchResults', true);
                        }
                    }
                });
            }

            return ajaxRequest;
        },

        applySort : function() {
            var sortType = $('.sort-dropdown').val();
            var order = $('.sort-order-dropdown').val();

            if (!this.model.get("isSearching")) {
                console.log("not searching");
                window.location = "#/tracks/" + this.model.get("page") + "?sort=" + sortType + "&order=" + order;
            } else {
                window.location = "#/tracks/search/" + this.model.get('searchTerms') + "?sort=" + sortType + "&order=" + order;
            }
        },

        searchTracks: function(e) {
            var search_terms = $('#track-search-input').val();
            var cleanedSearchTerms = encodeURI(search_terms).replace(/[^a-zA-Z0-9-_]/g, "").replace(/20/g, "+");
            this.model.set('searchTerms', cleanedSearchTerms);
            console.log("Searching tracks: " + search_terms);
            if (search_terms !== '') {
                window.location = "#/tracks/search/" + cleanedSearchTerms + "?sort=" + this.model.get('sortType') + "&order=" + this.model.get('sortOrder');
            }
        }

    });

    return TracksView;

});