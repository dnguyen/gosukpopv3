define([
    'jquery',
    'underscore',
    'backbone',
    'views/tracks/AddTrackToPlaylistModalView',
    'text!templates/TrackItemTemplate.html'
], function($, _, Backbone, AddTrackToPlaylistModalView, TrackItemTemplate) {

    var TrackItemView = Backbone.View.extend({
        tagName: 'tr',
        template: _.template(TrackItemTemplate),
        events: {
            'mouseenter' : 'showTrackOptions',
            'mouseleave' : 'hideTrackOptions',
            'click .add-to-favorite' : 'addToFavorites',
            'click .add-to-playlist' : 'addToPlaylist'
        },

        initialize: function() {
            // TODO: Move cleaning to track model...
            var trackName = encodeURI(this.model.get('title')).replace(/%20/g, "+").replace(/[^a-zA-Z0-9-_]/g, "");
            this.model.set('cleanedTitle', trackName);

            var cleanedArtistName = this.model.get('artistName').replace(/%20/g, "+").replace(/[^a-zA-Z0-9-_]/g, "");
            if (cleanedArtistName === '')
                cleanedArtistName = "EABDF24";
            this.model.set('cleanedArtistName', cleanedArtistName);
        },

        render: function() {
            $(this.el).attr('class', 'track-row').attr('data-track-id', this.model.get('id')).attr('title', this.model.get('title')).html(this.template(this.model.toJSON()));
            return this;
        },

        showTrackOptions: function(e) {
            var trackId = $(e.currentTarget).attr('data-track-id');
            $('#track-option-' + trackId + ' div').show();
            // Store track id to remove the options when not hovering over track anymore.
            this.prevTrackId = trackId;
        },

        hideTrackOptions: function(e) {
            $('#track-option-' + this.prevTrackId + ' div').hide();
        },

        addToFavorites: function(e) {
            e.stopImmediatePropagation();
        },

        addToPlaylist: function(e) {
            e.stopImmediatePropagation();
            console.log("Add to playlist");
            var addTrackToPlaylistModal = new AddTrackToPlaylistModalView( { model: this.model });
            addTrackToPlaylistModal.show();
        }

    });

    return  TrackItemView;
});