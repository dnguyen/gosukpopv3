define([
    'jquery',
    'underscore',
    'backbone',
    'models/UserModel',
    'views/HeaderView',
    'views/common/common',
    'pnotify'
], function($, _, Backbone, UserModel, HeaderView, common) {
    var AppView = Backbone.View.extend({

        initialize: function() {
            console.log("app.js init");

            this.eventAggregator = _.extend({}, Backbone.Events);
            this.eventAggregator.on('domchange:title', this.onDomChangeTitle, this);
            this.base_title = "gosukpop - ";
            $.pnotify.defaults.history = false;
        },

        el: '#app',

        switchMainView: function(view) {
            console.log("Switch main view");

            // Make sure the current view is completely destroyed before changing.
            if (typeof(this.main) != 'undefined') {
                this.main.undelegateEvents();
                this.main.$el.removeData().unbind();

                this.main.remove();
                Backbone.View.prototype.remove.call(this.main);
                console.log("Destroying current view");
            }

            // Check login status before loading new view.
            var this_ = this;
             $.when($.ajax({
                type: "GET",
                url: "http://10.0.0.6/gosukpop-api/public/session",
                dataType: 'json',
                error: function() {
                    console.log("error");
                }
            }).then(function(data) {
                // Create new UserModel if one does not alredy exist (on first page load)
                if (typeof(this_.userModel) == 'undefined') {
                    var user = new UserModel({
                        username: data.username,
                        loggedin: data.loggedin
                    });
                    this_.userModel = user;
                } else {
                    console.log(data);
                    this_.userModel.set('loggedin', data.loggedin);
                    this_.userModel.set('username', data.username);
                }

                this_.$header = new HeaderView({model : this_.userModel });
                this_.render();
                this_.main = view;
                this_.main.render();
            }));
        },

        render: function() {
            $('body').prepend(this.$header.render().el);
        },

        onDomChangeTitle: function (title) {
            $(document).attr('title', title);
        }

    });

    return AppView;
});