define([
    'jquery',
    'underscore',
    'backbone',
    'models/ArtistModel',
    'models/TrackModel',
    'collections/TracksCollection',
    'views/tracks/TrackItemView',
    'text!templates/SingleArtistTemplate.html',
    'text!templates/ArtistTrackItemTemplate.html'
], function($, _, Backbone, ArtistModel, TrackModel, TracksCollection, TrackItemView, SingleArtistTemplate, ArtistTrackItemTemplate) {

    var SingleArtistView = Backbone.View.extend({

        template: _.template(SingleArtistTemplate),

        initialize: function(name, artistid) {
            console.log("SingleArtistView init");

            this.tracksByArtist = new TracksCollection();

            var this_ = this;
            $.ajax({
                type: "POST",
                url: "artist/get",
                data: { id : artistid },
                dataType: 'json',
                success: function(data) {
                    console.log(data);
                    var artistModel = new ArtistModel({
                        id : data.artist.id,
                        name : data.artist.name,
                        description : data.artist.description,
                        artist_tracks : data.artist_tracks
                    });
                    this_.model = artistModel;

                    _.each(data.artist_tracks, function(track) {
                        this_.tracksByArtist.add(
                            new TrackModel({
                                id : track.id,
                                artist: track.artist,
                                title : track.title,
                                videoid : track.videoId
                            })
                        );
                    });

                    window.AppView.eventAggregator.trigger('domchange:title', window.AppView.base_title + this_.model.get('name'));
                }
            });


        },

        render: function() {
            console.log("singleartist render");
            $(this.el).attr('id', 'single-artist-view').html(this.template(this.model.toJSON()));
            this.renderTracksByArtist();
            $('#main-content').append(this.el);
        },

        renderTracksByArtist: function() {
            console.log("render tracks by artist");
           /* var this_ = this;
            _.each(this_.tracksByArtist.models, function(track) {
                var TrackItem = new TrackItemView({ model: track });
                TrackItem.template = _.template(ArtistTrackItemTemplate);
                $(this_.el).find('#tracks-table tbody').append(TrackItem.render().el);
            });*/
        }

    });

    return SingleArtistView;

});