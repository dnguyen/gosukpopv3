define([
    'jquery',
    'underscore',
    'backbone',
    'text!templates/ArtistItemTemplate.html'
], function($, _, Backbone, ArtistItemTemplate) {

    var ArtistItemView = Backbone.View.extend({
        tagName: 'li',
        template: _.template(ArtistItemTemplate),

        events: {
            'click' : 'goToArtist'
        },

        render: function() {
            $(this.el).attr('class', 'artist-item').attr('data-artist-id', this.model.get('id')).attr('title', this.model.get('name')).html(this.template(this.model.toJSON()));
            return this;
        },

        goToArtist: function() {
            var id = $(this.el).attr('data-artist-id');
            var name = $(this.el).attr('title').replace(/%20/g, "+").replace(/[^a-zA-Z0-9-_]/g, "");
            if (name == '')
                name = "EABDF24";
            window.location.href = '#/artist/'+name+'/'+id;
        }
    });

    return ArtistItemView;
});