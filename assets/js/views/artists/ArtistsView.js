define([
    'jquery',
    'underscore',
    'backbone',
    'models/ArtistModel',
    'collections/ArtistsCollection',
    'views/artists/ArtistItemView',
    'text!templates/ArtistsTemplate.html'
], function($, _, Backbone, ArtistModel, ArtistsCollection, ArtistItemView, ArtistsTemplate) {

    var ArtistsView = Backbone.View.extend({

        tagName: 'section',
        template: _.template(ArtistsTemplate),
        events: {
            'click #next-btn' : 'nextArtistsPage',
            'click #prev-btn' : 'prevArtistsPage',
            'click #filter-btn' : 'showFilters',
            'click #artists-search-btn' : 'searchArtists',
            'change .sort-dropdown' : 'applySort',
            'change .sort-order-dropdown' : 'applySort'
        },

        initialize: function(options) {
            console.log("ArtistsView init");
            window.AppView.eventAggregator.trigger('domchange:title', window.AppView.base_title + "Artists");

            this.Artists = new ArtistsCollection();
            this.model = new Backbone.Model();

            this.model.set('isSearching', options.isSearching);
            this.model.set('searchTerms', options.searchTerms);

            this.model.set('sortType', options.sortType);
            this.model.set('sortOrder', options.order);
            this.model.set('filterGender', options.gender);
            this.model.set('filterGroupType', options.artistType);

            if (options.page > 0) {
                this.model.set('page', parseInt(options.page, 10));
            } else {
                this.model.set('page', 1);
            }
        },

        render: function() {
            var this_ = this;
            $.when(this_.prepareData()).done(function() {
                $(this_.el).attr('id', 'artists-view').html(this_.template(this_.model.toJSON()));
                $('#main-content').append(this_.el);
                this_.renderArtistsCollection();
            });
        },

        renderArtistsCollection: function() {
            console.log(this.Artists);
            var container = document.createDocumentFragment();
            $('.artists-list').html('');
            this.Artists.forEach(function(artist) {
                var ArtistItem = new ArtistItemView({ model: artist });
                container.appendChild(ArtistItem.render().el);
            });
            $('.artists-list').append(container);
        },

        prepareData: function() {
            var this_ = this;
            var ajaxRequest;

            if (!this.model.get('isSearching')) {
                ajaxRequest = $.ajax({
                    type: "GET",
                    url: "http://10.0.0.6/gosukpop-api/public/artists",
                    data: {
                        page : this_.model.get('page'),
                        sort : this_.model.get('sortType'),
                        order : this_.model.get('sortOrder'),
                        gender : this_.model.get('filterGender'),
                        type : this_.model.get('filterGroupType')
                    },
                    dataType: 'json',
                    success: function(data) {
                        this_.model.set('pageCount', data.pageCount);
                        _.each(data.artists, function(artist) {
                            var newArtist = new ArtistModel ({
                                    id : artist.id,
                                    name : artist.name
                            });
                            var ArtistItem = new ArtistItemView({ model: newArtist });
                            this_.Artists.add(newArtist);
                        });
                    },
                    error: function(data) {
                        console.log("error");
                    }
                });
            } else {
                console.log(this_.model.get('searchTerms'));
                this_.Artists.reset();
                ajaxRequest = $.ajax({
                    type: 'GET',
                    url: 'http://10.0.0.6/gosukpop-api/public/artists/search/' + this_.model.get('searchTerms'),
                    dataType: 'json',
                    success: function(data) {
                        console.log(data);
                        this_.Artists.reset();
                        _.each(data, function(artist) {
                            var newArtist = new ArtistModel ({
                                    id : artist.id,
                                    name : artist.name
                            });
                            this_.Artists.add(newArtist);
                        });
                    }
                });
            }

            return ajaxRequest;
        },

        nextArtistsPage: function() {
            this.model.set('page', this.model.get('page') + 1);
            window.location.href = "#/artists/" + this.model.get('page');
        },

        prevArtistsPage: function() {
            this.model.set('page', this.model.get('page') - 1);
            if (this.model.get('page') <= 0) {
                this.model.set('page', 1);
            }
            window.location.href = "#/artists/" + this.model.get('page');
        },

        showFilters: function() {
            jQuery(".filters-container").toggle();
        },

        searchArtists: function() {
            var searchKeywords = $('#artists-search-input').val();
            // Strip keywords of any symbols, then replace all spaces with a + character
            var cleanedSearchTerms = encodeURI(searchKeywords).replace(/[^0-9a-zA-Z]+/g, "").replace(/20/, "+");
            if (searchKeywords !== '')
                window.location.href = "#/artists/search/" + cleanedSearchTerms + "?sort=" + this.model.get("sortType") + "&" + this.model.get('sortOrder');
        },

        applySort: function() {
            var sortType = $('.sort-dropdown').val();
            var order = $('.sort-order-dropdown').val();

            if (!this.model.get("isSearching")) {
                console.log("not searching");
                window.location = "#/artists/" + this.model.get("page") + "?sort=" + sortType + "&order=" + order;
            } else {
                window.location = "#/artists/search/" + this.model.get('searchTerms') + "?sort=" + sortType + "&order=" + order;
            }
        }
    });

    return ArtistsView;

});