define([
    'jquery',
    'underscore',
    'backbone',
    'text!templates/NewReleaseTemplate.html'
], function($, _, Backbone, newReleaseTemplate) {

    var NewReleaseView = Backbone.View.extend({
        tagName: 'li',

        template: _.template(newReleaseTemplate),

        render: function() {
            $(this.el).html(this.template( this.model.toJSON()));
            return this;
        }
    });
    return NewReleaseView;
});