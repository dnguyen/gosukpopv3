define([
    'jquery',
    'underscore',
    'backbone',
    'collections/TracksCollection',
    'text!templates/PopularTrackTemplate.html'
], function($, _, Backbone, TracksCollection, PopularTrackTemplate) {

    var NewReleaseView = Backbone.View.extend({
        tagName: 'li',

        template: _.template(PopularTrackTemplate),

        render: function() {
            $(this.el).html(this.template(this.model.toJSON()));
            return this;
        }
    });
    return NewReleaseView;
});