define([
    'jquery',
    'underscore',
    'backbone',
    'models/UserModel',
    'text!templates/LoginTemplate.html'
], function($, _, Backbone, UserModel, LoginTemplate) {

    var LoginView = Backbone.View.extend({

        tagName: 'section',
        template: _.template(LoginTemplate),

        events: {
            'click #sign-in-btn' : 'trySignIn'
        },

        initialize: function() {
            console.log("loginviewinit");
            window.AppView.eventAggregator.trigger('domchange:title', window.AppView.base_title + "Login");

            var this_ = this;
             $.when($.ajax({
                type: "GET",
                url: "http://10.0.0.6/gosukpop-api/public/session",
                dataType: 'json',
                error: function() {
                    console.log("error");
                }
            }).then(function(output) {
                var user = new UserModel({
                    loggedin: output.loggedin
                });
                if (user.get('loggedin'))
                    window.location = "#/";
            }));
        },

        render: function() {
            $(this.el).attr('id', 'login-view').html(this.template( { }));
            $('#main-content').append(this.el);
        },

        trySignIn: function() {

            var username_input = $('#username-textbox').val();
            var password_input = $('#password-textbox').val();

            var loginRequest = $.ajax({
                type: "GET",
                url: "http://10.0.0.6/gosukpop-api/public/auth",
                data: {
                    username : username_input,
                    password: password_input
                },
                dataType: 'json'
            });

            $.when(loginRequest).done(function(data) {
                if (data.status) {
                    window.location = "#/";
                } else {
                    $('#login-errors').show();
                    $('#login-errors').html(data.error);
                }
            });
        }
    });

    return LoginView;
});