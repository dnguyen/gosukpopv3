define([
    'jquery',
    'underscore',
    'backbone',
    'pnotify',
    'models/UserModel',
    'text!templates/RegisterTemplate.html'
], function($, _, Backbone, pnotify, UserModel, RegisterTemplate) {

    var RegisterView = Backbone.View.extend({

        template: _.template(RegisterTemplate),
        events: {
            'click #sign-up-btn' : 'tryRegister',
            'blur #username-textbox' : 'validateUsername',
            'blur #password-textbox' : 'validatePassword',
            'blur #confirm-password-textbox' : 'validateConfirmPassword'
        },

        render: function() {
            $(this.el).attr('id', 'register-view').html(this.template());
            $('#main-content').append(this.el);
        },

        tryRegister: function(e) {
            var registerData = {
                username : $('#username-textbox').val(),
                password : $('#password-textbox').val(),
                confirm_password : $('#confirm-password-textbox').val()
            };
            if (this.validateForm()) {
                var registerRequest = $.ajax({
                    type: 'POST',
                    url: 'http://10.0.0.6/gosukpop-api/public/users',
                    data : registerData,
                    dataType: 'json'
                });
                $.when(registerRequest).done(function(data) {
                    if (data.status) {
                        window.location = "#/";

                        $.pnotify({
                            title: 'Sign up complete!',
                            text: 'You have successfully signed up and have been logged in.',
                            type: 'success',
                            delay: 3000
                        });
                    }
                });
            }
        },

        validateForm: function() {
            if (this.validateUsername() && this.validatePassword() && this.validateConfirmPassword()) {
                return true;
            } else {
                return false;
            }
        },

        displayTextboxError: function(textbox, errorMessage) {
            var TextBox = $('#' + textbox + '-textbox');
            TextBox.parent().find('span').each(function() { $(this).remove(); });
            TextBox.parent().parent().attr('class', 'control-group error');
            TextBox.after('<span class="help-inline">' + errorMessage + '</span>');
        },

        displaySuccessTextbox: function(textbox) {
            var TextBox = $('#' + textbox + '-textbox');
            TextBox.parent().find('span').each(function() { $(this).remove(); });
            TextBox.parent().parent().attr('class', 'control-group success');
        },

        validateUsername: function(e) {
            var usernameTextBox = $('#username-textbox');
            if (usernameTextBox.val() === '') {
                this.displayTextboxError('username', "Please enter a username");
                return false;
            } else {
                this.displaySuccessTextbox('username');
                return true;
            }
        },

        validatePassword: function(e) {
            var passwordTextBox = $('#password-textbox');
            if (passwordTextBox.val() === '') {
                this.displayTextboxError('password', "Please enter a password");
                return false;
            } else {
                this.displaySuccessTextbox('password');
                return true;
            }
        },

        validateConfirmPassword: function(e) {
            var confirmPasswordTextBox = $('#confirm-password-textbox');
            var passwordTextBox = $('#password-textbox');
            var error = false;
            var errorMessage = "";

            if (confirmPasswordTextBox.val() === '') {
                error = true;
                errorMessage = "Please confirm your password";
            }

            if (confirmPasswordTextBox.val() != passwordTextBox.val()) {
                error = true;
                errorMessage = "Passwords do not match";
            }

            if (error) {
                this.displayTextboxError('confirm-password', errorMessage);
                return false;
            } else {
                this.displaySuccessTextbox('confirm-password');
                return true;
            }
        }
    });

    return RegisterView;

});