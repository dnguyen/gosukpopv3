/*
gosulib.js
Common functions used in gosukpopv3
dependencies:
    jquery
    underscore
 */

define([
    'jquery',
    'underscore',
    'collections/TracksCollection',
    'models/TrackModel'
    ], function($, _, TracksCollection, TrackModel) {
        var gosu = {

            GetTrackSearchResults : function(collection, search_terms) {
                console.log("Get track search results fnct in gosulib");
                var results = new TracksCollection();
                var searchTerms = search_terms.split("+");
                console.log(searchTerms + " results: ");
                _.each(collection.models, function(item) {
                    _.each (searchTerms, function(term) {
                        if (item.get('title').toLowerCase().match(term.toLowerCase()) !== null || (item.get('artistName').toLowerCase().match(term.toLowerCase()) !== null)) {
                            //console.log(item.get('title') + " - " + item.get('artistName'));
                            results.add(item);
                        }
                    });
                });

                collection.reset(results.models);

                return collection;
            },

            // Converts tracks data from server to a collection
            GetTracksCollectionFromArray : function(tracks) {
                var tracksCollection = new TracksCollection();

                _.each(tracks, function(track) {
                    var newTrack = new TrackModel ({
                            id : track.trackId,
                            title : track.title,
                            shortenedTitle : track.title,
                            artist : track.artistId,
                            artistName : track.name,
                            uploaded : track.uploaded,
                            videoId : track.videoId,
                            viewCount : track.viewCount
                    });
                    tracksCollection.add(newTrack);
                });
                return tracksCollection;
            }
        };

        return gosu;
    }
);
