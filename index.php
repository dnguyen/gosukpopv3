<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>gosukpop</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <link href='http://fonts.googleapis.com/css?family=Droid+Sans' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>

        <link href="assets/stylesheets/screen.css" media="screen, projection" rel="stylesheet" type="text/css" />
        <link href="assets/stylesheets/print.css" media="print" rel="stylesheet" type="text/css" />
        <!--[if IE]>
            <link href="/stylesheets/ie.css" media="screen, projection" rel="stylesheet" type="text/css" />
        <![endif]-->

        <link rel="stylesheet" href="assets/css/bootstrap.css" />
        <link rel="stylesheet" href="assets/css/jquery.pnotify.default.css" />
        <link rel="stylesheet" href="assets/css/jquery.pnotify.default.icons.css" />
        <link rel="stylesheet" href="assets/css/font-awesome.min.css" />
        <link rel="stylesheet" href="assets/css/normalize.min.css">
        <link rel="stylesheet" href="assets/stylesheets/styles.css">
        <script data-main="assets/js/main" src="assets/js/lib/require/require.js"></script>
    </head>

    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.</p>
        <![endif]-->
        <section id="app">
            <section id="header">

            </section>
            <div id="main-content" class="row">

            </div>

        </section>
        <div id="footer">
            <div id="footer-wrapper" class="row">
                <div id="footer-logo" class="span4 line">
                    <a href="#"><img src="assets/img/logo-footer.png"/></a>
                </div>
                <div id="footer-navigation" class="span4 line">
                    <h4>Site Links</h4>
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Explore</a></li>
                        <li><a href="#">Sign Up</a></li>
                        <li><a href="#">About</a></li>
                        <li><a href="#">Contact</a></li>
                    </ul>
                </div>
                <div id="social-links" class="span4">
                    <h4>Follow Us</h4>
                    <ul>
                        <li><img class="social-icon" src="assets/img/facebook.png"/><a href="">Be a fan on Facebook</a></li>
                        <li><img class="social-icon" src="assets/img/twitter.png"/><a href="">Follow us on Twitter</a></li>
                        <li><img class="social-icon" src="assets/img/feed.png"/><a href="">Subscribe to our blog</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </body>
</html>

